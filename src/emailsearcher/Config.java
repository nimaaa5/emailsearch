/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package emailsearcher;

/**
 *
 * @author mortadda
 */
public class Config {

    public final static String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36";
    public final static int TIME_OUT = 5000;
    public final static String EMAIL_PATTERN = "[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+";
    public final static String KEY = "AIzaSyA4-NNR1uCIxUpq4aEo2RmSgWJN2M1QtmE";
    public final static String CX = "003926075187911630200:svkxo6ffgco";
//  public final static String [] DOMAIN ={"ar", "hy", "be", "bg", "ca", "hr", "cs", "da", "nl", "en", "eo", "et", "tl", "fi", "fr", "de", "el", "iw", "hu", "is", "id", "it", "ja", "ko", "lv", "lt", "no", "fa", "pl", "pt", "ro", "ru", "sr", "sk", "sl", "es", "sv", "th", "tr","uk", "vi","zh-CN","zh-TW"};
    public final static String[] KEYWORDS_COMPANY = {"شركة", "ընկերություն", "кампанія", "компания", "empresa", "společnost", "selskab", "bedrijf", "company", "Kompanio", "ettevõte", "kompanya", "yhtiö", "compagnie", "Unternehmen", "εταιρεία", "חֶברָה", "vállalat", "Félagið", "perusahaan", "società", "会社", "회사", "kompānija", "bendrovė", "selskap", "شرکت", "firma", "companhia", "companie", "Компания", "компанија", "spoločnosť", "podjetje", "empresa", "företag", "บริษัท", "şirket", "компанія", "Công ty", "公司", "公司"};
    public final static String[] KEYWORD_CONTACT = {"اتصال", "Կապ", "кантакт", "контакт", "contacte", "Kontakt", "kontakt", "contact", "contact", "kontakton", "kontakt", "contact", "ottaa yhteyttä", "contact", "Kontakt", "επαφή", "Translate", "contact", "איש קשר", "kapcsolatba lépni", "tengilið", "kontak", "contatto", "接触", "kontakts", "kontaktas", "kontakt", "تماس", "kontakt", "a lua legatura", "контакт", "контакт", "kontakt", "kontakt", "contacto", "kontakta", "ติดต่อ", "temas", "контакт", "tiếp xúc", "联系", "聯繫"};
}
