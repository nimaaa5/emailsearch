package emailsearcher;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Mortada
 */
import java.sql.*;
import javax.swing.*;

public class JavaConnect {

    Connection conn;

    public static Connection ConnecerDB() {

        try {
            Class.forName("org.sqlite.JDBC");//for my sql ("com.mysql.jdbc.Driver")
            Connection conn = DriverManager.getConnection("jdbc:sqlite:searchengine.sqlite"); 
            return conn;
        } catch (ClassNotFoundException | SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return null;
        }

    }
}
