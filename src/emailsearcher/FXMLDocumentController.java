package emailsearcher;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

/**
 *
 * @author Abolfazl
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    Label lblControl;

    @FXML
    Label lblResult;

    @FXML
    Label lblWords;

    @FXML
    JFXButton btnExportFileExecel;

    @FXML
    ProgressBar progProgress;

    @FXML
    Label lblProgres;

    @FXML
    Label lblSearch;

    @FXML
    Label lblTotalSearch;

    @FXML
    Label lblWordNum;

    @FXML
    Label lblWord;

    @FXML
    JFXButton btnStart;

    @FXML
    Label lblFile;

    @FXML
    JFXButton btnImportFile;

    @FXML
    Label lblFileName;

    @FXML
    Label lblLanguage;

    @FXML
    Label lblCountry;

    @FXML
    Label lblSearchType;

    @FXML
    Label lblShow;

    @FXML
    JFXComboBox cmbLanguage;

    @FXML
    JFXComboBox cmbCountry;

    @FXML
    JFXRadioButton rdbOneByOne;

    @FXML
    JFXRadioButton rdbAllInOne;

    @FXML
    JFXRadioButton rdbAll;

    @FXML
    JFXRadioButton rdbOnlySuccess;

    @FXML
    JFXButton btnNewWord;

    @FXML
    JFXButton btnSelectAll;

    @FXML
    JFXListView<JFXCheckBox> listWords;

    FileChooser fileChooser;

    @FXML
    JFXListView<JFXTreeTableView<Result>> listTables;

    ArrayList<JFXCheckBox> words;

    ObservableList<Result> results;

    @FXML
    private void btnExportFileEcele_OnClick() {
        Excel.write(results);
    }

    @FXML
    private void btnStart_OnClick() {
        if (rdbOneByOne.isSelected()) {
            words.stream().filter((word) -> (word.isSelected())).forEachOrdered((_item) -> {
                createTable(_item.getText());
            });
        } else if (rdbAllInOne.isSelected()) {
            String totalword = new String();
            for (JFXCheckBox word : words) {
                if (word.isSelected()) {
                    totalword += word.getText();
                }
            }
            createTable(totalword);
        }

    }

    @FXML
    private void btnImportFile_OnClick() throws IOException, InvalidFormatException {

        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Excel Files", "*.xlsx"));

        fileChooser.setTitle("Open File");
        File ChoosedFile = fileChooser.showOpenDialog(btnImportFile.getScene().getWindow());
        if (ChoosedFile != null) {
            ArrayList<String> productsName = Excel.read(ChoosedFile);
            lblFileName.setText(ChoosedFile.getName());
            for (String word : productsName) {
                //to read from choosed fill and fill words
                JFXCheckBox foo = new JFXCheckBox(word);
                foo.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    lblWordNum.setText("" + (Integer.parseInt(lblWordNum.getText()) + 1));
                });
                this.words.add(foo);
            }
            FillWordList(this.words);
        }
    }

    @FXML
    private void btnNewWord_OnClick() {
        TextInputDialog dialog = new TextInputDialog("Word");
        dialog.setTitle("Word Input Dialog");
        dialog.setHeaderText("Look, a Word Input Dialog");
        dialog.setContentText("Please enter your word:");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            if (!result.get().isEmpty()) {
                JFXCheckBox foo = new JFXCheckBox(result.get());
                foo.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    lblWordNum.setText("" + (Integer.parseInt(lblWordNum.getText()) + 1));
                });
                this.addItemToWordsList(foo);
                this.words.add(foo);
            }
        }
    }

    @FXML
    private void btnSelectAll_OnClick() {
        this.words.forEach((word) -> {
            word.setSelected(true);
        });
    }

    private void FillWordList(ArrayList<JFXCheckBox> words) {
        this.listWords.getItems().clear();
        words.forEach((word) -> {
            this.addItemToWordsList(word);
        });
    }

    private void addItemToWordsList(JFXCheckBox item) {
        this.listWords.getItems().add(item);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        this.fileChooser = new FileChooser();
        this.words = new ArrayList<>();

        ToggleGroup tgpSearchType = new ToggleGroup();
        rdbAllInOne.setToggleGroup(tgpSearchType);
        rdbAllInOne.setSelected(true);
        rdbOneByOne.setToggleGroup(tgpSearchType);

        ToggleGroup tgpShow = new ToggleGroup();
        rdbAll.setToggleGroup(tgpShow);
        rdbOnlySuccess.setToggleGroup(tgpShow);
        rdbOnlySuccess.setSelected(true);

        for (String Country : DBConection.Countrys()) {
            cmbCountry.getItems().add(Country);

        }

        for (String Language : DBConection.Languages()) {
            cmbLanguage.getItems().add(Language);

        }

        this.results = FXCollections.observableArrayList();
    }

    private void createTable(String text) {
//        ObservableList<Result> results = EmailsParser.getEmails(text,
//                cmbLanguage.getSelectionModel().getSelectedItem().toString(),
//                cmbCountry.getSelectionModel().getSelectedItem().toString(),
//                true,rdbOnlySuccess.isSelected());

        ObservableList<Result> results = FXCollections.observableArrayList();
        for (int i = 0; i < 30; i++) {
            results.add(new Result(new SimpleStringProperty("word" + i),
                    new SimpleStringProperty("web" + i),
                    new SimpleStringProperty("email" + i)));
        }
        this.results.addAll(results);

        JFXTreeTableColumn<Result, String> wordColumn = new JFXTreeTableColumn<>("Word");
        wordColumn.setPrefWidth(150);
        wordColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<Result, String> param) -> {
            if (wordColumn.validateValue(param)) {
                return param.getValue().getValue().word;
            } else {
                return wordColumn.getComputedValue(param);
            }
        });

        JFXTreeTableColumn<Result, String> urlColumn = new JFXTreeTableColumn<>("WebURL");
        urlColumn.setPrefWidth(150);
        urlColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<Result, String> param) -> {
            if (urlColumn.validateValue(param)) {
                return param.getValue().getValue().WebURL;
            } else {
                return urlColumn.getComputedValue(param);
            }
        });

        JFXTreeTableColumn<Result, String> emailColumn = new JFXTreeTableColumn<>("Email");
        emailColumn.setPrefWidth(150);
        emailColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<Result, String> param) -> {
            if (emailColumn.validateValue(param)) {
                return param.getValue().getValue().Email;
            } else {
                return emailColumn.getComputedValue(param);
            }
        });

        TreeItem<Result> root = new RecursiveTreeItem<Result>(results, RecursiveTreeObject::getChildren);
        JFXTreeTableView<Result> treeView = new JFXTreeTableView<Result>(root, results);
        treeView.setShowRoot(false);
        treeView.setEditable(true);
        treeView.getColumns().setAll(wordColumn, urlColumn, emailColumn);

        JFXTextField filterField = new JFXTextField();
        filterField.textProperty().addListener((o, oldVal, newVal) -> {
            treeView.setPredicate(user -> user.getValue().word.get().contains(newVal)
                    || user.getValue().WebURL.get().contains(newVal)
                    || user.getValue().Email.get().contains(newVal));
        });

        Label size = new Label();
        size.textProperty().bind(Bindings.createStringBinding(() -> treeView.getCurrentItemsCount() + "",
                treeView.currentItemsCountProperty()));

        listTables.getItems().add(treeView);
    }

}
