/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package emailsearcher;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.customsearch.Customsearch;
import com.google.api.services.customsearch.model.Search;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author mortadda
 */
public class GoogleParser {

    private final String query;
    private final String lang;
    private final boolean deep_search;
    private final EmailsParser emailHunter;

    public GoogleParser(String query, String lang, String country, boolean deep_search) {
        this.query = query;
        this.lang = lang;
        this.deep_search = deep_search;
        emailHunter = new EmailsParser();
    }

    protected ArrayList<String> getLinks() {
        ArrayList<String> Links = new ArrayList<>();
        Search result = null;
        Customsearch.Cse.List list;
        HttpRequestInitializer httpRequestInitializer = new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest hr) throws IOException {
            }
        };

        JsonFactory jsonFactory = new JacksonFactory();
        Customsearch custom = new Customsearch(new NetHttpTransport(), jsonFactory, httpRequestInitializer);
        try {
            list = custom.cse().list(this.query);
            list.setCx(Config.CX);
            list.setKey(Config.KEY);
            result = list.execute();
            for (int i = 1; i < 6; i++) {
                result.getItems().stream().forEach((result1) -> {
                    System.out.println("res   " + result1.getLink());
                    Links.add(result1.getLink());
                });
                list.setStart(new Long(i * 10));
                result = list.execute();
            }
        } catch (IOException e) {
            System.out.println("eroore" + e);
        }
        return Links;

    }
}
