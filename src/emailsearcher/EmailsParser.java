/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package emailsearcher;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author mortadda
 */
public class EmailsParser {

    public EmailsParser() {

    }

    public static ObservableList<Result> getEmails(String query, String lang, String country, boolean is_recursive, boolean onlysucess) {

        ObservableList<Result> results = FXCollections.observableArrayList();
        GoogleParser gp = new GoogleParser(query, lang, country, is_recursive);
        for (String nextLink : gp.getLinks()) {
            results.addAll(Parese(query, nextLink, is_recursive, onlysucess));
        }
        return results;
    }

    public static ObservableList<Result> Parese(String query, String link, boolean is_recursive, boolean onlysucess) {

        Document doc;
        Set<String> emails;
        ObservableList<Result> results = FXCollections.observableArrayList();

        System.out.println("\n-----------------------" + link + "--------------------------");
        try {
            doc = Jsoup.connect(link)
                    .userAgent(Config.USER_AGENT)
                    .timeout(Config.TIME_OUT).get();
            Pattern p = Pattern.compile(Config.EMAIL_PATTERN);

            Matcher matcher = p.matcher(doc.text());
            /* get contact  url recersive now  80% of site contact us url into its for ex: facevook.com/contact-us.php */
            if (!is_recursive) {
                Elements links = doc.select("a[href*=contact]");
                for (Element l : links) {
                    try {
                        results.addAll(Parese(query, l.absUrl("href"), true, onlysucess));
                    } catch (Exception e) {
                        System.out.println("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
                    }
                }
            }
            //TODO add language recersive search in dom ,,,,ex: تماس.... اتصل ,,,,,,,
            /* end get contact  url recersive  */
            emails = new HashSet<String>();

            while (matcher.find()) {
                emails.add(matcher.group());
            }
            System.out.println("emails   :  " + emails);
            if (!onlysucess || !emails.isEmpty()) {
                results.add(new Result(new SimpleStringProperty(query),
                        new SimpleStringProperty(link), new SimpleStringProperty(emails.toString())));
            }

        } catch (IOException ex) {
            /*time out ,internt problem ,server error*/
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("\n-------------------------------------------------------");

        return results;
    }
}
